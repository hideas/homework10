<?php
require ("config.php");
require ("functions.php");

$posts = get_statti();

?>

<? include "inc/header.php";?>
<div id="content">
    <div id="main">
        <h1>Таблица статей</h1>
        <table border="2">
            <tr>
                <td><b>Заголовок</b></td>
                <td><b>Дата</b></td>
                <td><b>Изображение</b></td>
                <td><b>Описание</b></td>
            </tr>
            <? foreach ($posts as $item) :?>
            <tr>
                <td><?=$item['title'];?></td>
                <td><?=$item['date'];?></td>
                <td><img align="left" src="<?=$item['img_src'];?>"></td>
                <td><?=$item['discription'];?></td>
            </tr>
        <? endforeach; ?>
        </table>
        <p>
            <a href="admin.php">Назад</a>
        </p>
        <p>
            <a href="stati_add.php">Добавить запись</a>
        </p>
    </div>
<? include "inc/sidebar.php";?>

<? include "inc/footer.php";?>