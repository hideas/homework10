<?php
function get_statti() {
    global $db;
    $sql = "SELECT * FROM statti";
    $result = mysqli_query($db, $sql);

    if (!$result) {
        exit(mysqli_error());
    }

    for ($i = 0; $i < mysqli_num_rows($result); $i++) {
        $row[] = mysqli_fetch_array($result);
    }
    return $row;
}

function get_users() {
    global $db;
    $sql = "SELECT * FROM users";
    $result = mysqli_query($db, $sql);

    if (!$result) {
        exit(mysqli_error());
    }

    for ($i = 0; $i < mysqli_num_rows($result); $i++) {
        $row[] = mysqli_fetch_array($result);
    }
    return $row;
}

function registration($post){
    $login = clean_data($post['reg_login']);
    $password = trim($post['reg_password']);
    $conf_pass = trim($post['reg_password_confirm']);
    $email = clean_data($post['reg_email']);
    $name = clean_data($post['reg_name']);

    $msg = '';
    if (empty($login)){
        $msg .="Введите логин! <br />";
    }
    if (empty($password)){
        $msg .="Введите пароль! <br />";
    }
    if (empty($email)){
        $msg .="Введите email! <br />";
    }
    if (empty($name)){
        $msg .="Введите имя! <br />";
    }

    if ($msg){
        $_SESSION['reg']['login'] = $login;
        $_SESSION['reg']['email'] = $email;
        $_SESSION['reg']['name'] = $name;
        return $msg;
    }


    if($conf_pass == $password) {
        global $db;

        $sql = "SELECT user_id
          FROM users
          WHERE login='%s'";
        $sql = sprintf($sql,mysqli_real_escape_string($db, $login));

        $result = mysqli_query($db, $sql);

        if(mysqli_num_rows($result) > 0) {
            $_SESSION['reg']['email'] = $email;
            $_SESSION['reg']['name'] = $name;

            return "Пользователь с таким логином уже существует";
        }
        $password = md5($password);
        $hash = md5(microtime());//текущая метка времени в милсекундах

        $query = "INSERT INTO users (
            name,
            email,
            password,
            login,
            hash
            ) 
          VALUES (
            '%s',
            '%s',
            '%s',
            '%s',
            '$hash'
          )";
        $query = sprintf($query,
            mysqli_real_escape_string($db, $name),
            mysqli_real_escape_string($db, $email),
            $password,
            mysqli_real_escape_string($db, $login)
        );
        $result2 = mysqli_query($db, $query);

        if(!$result2) {
            $_SESSION['reg']['login'] = $login;
            $_SESSION['reg']['email'] = $email;
            $_SESSION['reg']['name'] = $name;
            return "Ошибка при добавлении пользователя в базу данных".mysqli_error();
        }
        else{
            $headers = '';
            $headers .= "From: Admin <admin@ukr.net> \r\n";
            $headers .= "Content-Type: text/plain; charset=utf8";

            $tema = "registration";

            $mail_body = "Спасибо за регистрацию на сайте. "
                ."Ваша ссылка для подтверждения  учетной записи: "
                ."http://L10/confirm.php?hash=".$hash;

            mail($email,$tema,$mail_body,$headers);

            return TRUE;
        }
    }
    else {
        $_SESSION['reg']['login'] = $login;
        $_SESSION['reg']['email'] = $email;
        $_SESSION['reg']['name'] = $name;
        return "Вы не правильно подтвердили пароль";
    }
}

function clean_data($str) {
    return strip_tags(trim($str));
}

function confirm(){
    global $db;
    $new_hash = clean_data($_GET['hash']);

    $query = "UPDATE users SET confirm = '1' WHERE hash = '%s'";

    $query = sprintf($query, mysqli_real_escape_string($db, $new_hash));

    $result3 = mysqli_query($db, $query);

    if(mysqli_affected_rows($db) == 1) {
        return TRUE;
    } else {
        return "Неверный код подтверждения регистрации";
    }
}

function check_user(){
    return TRUE;
}

function login($post){
    if (empty($post['login']) || empty($post['password'])){
        $GLOBALS['$msg'] = "Заполните поля!!!";
        return FALSE;
    }

    $login = clean_data($post['login']);
    $password = md5(trim($post['password']));

    global $db;
    $sql = "SELECT user_id,confirm
      FROM users
      WHERE login = '%s'
      AND password = '%s'";
    $sql = sprintf($sql,mysqli_real_escape_string($db, $login),$password);

    $result = mysqli_query($db,$sql);

    if(!$result || mysqli_num_rows($result) < 1) {
        $GLOBALS['$msg'] = "Неправильный логин или пароль";
        return FALSE;
    }

    $data = mysqli_fetch_all($result, MYSQLI_ASSOC);

    if ($data[0]['confirm'] == 0){
        $GLOBALS['$msg'] = "Пользователь с таким логином еще не продтвержден";
        return FALSE;
    }

    $sess = md5(microtime());

    $_SESSION['sess'] = $sess;

    if ($post['member'] == 1){
        $time = time() + 10*24*60*60;

        setcookie('login',$login,$time);
        setcookie('password',$password,$time);
    }

    return TRUE;
}

function add_statti($post){
    $title = $post['title'];
    $author = $post['author'];
    $date = date('j-m-y');
    $img_src = $post['img_src'];
    $discription = $post['discription'];

    $msg = '';
    if (empty($title)){
        $msg .="Введите заголовок! <br />";
    }
    if (empty($author)){
        $msg .="Укажите автора! <br />";
    }
    if (empty($date)){
        $msg .="Введите дату! <br />";
    }
    if (empty($img_src)){
        $msg .="Укажите ссылку на картинку! <br />";
    }
    if (empty($discription)){
        $msg .="Введите описание! <br />";
    } else{
        global $db;

        $sql = "SELECT id
          FROM statti
          WHERE title='%s'";
        $sql = sprintf($sql,mysqli_real_escape_string($db, $title));

        $result = mysqli_query($db, $sql);

        if(mysqli_num_rows($result) > 0) {
            return "Такая запись уже существует";
        }

        $query = "INSERT INTO statti (
            title,
            meta_k,
            meta_d,
            author,
            date,
            img_src,
            mini_descr,
            discription
            ) 
          VALUES (
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s'
          )";
        $query = sprintf($query,
            $title,
            $title,
            $title,
            $author,
            $date,
            $img_src,
            $discription,
            $discription
        );
        $result2 = mysqli_query($db, $query);

        if(!$result2) {
            return "Ошибка при добавлении статьи в базу данных";
        }
        else{
            return TRUE;
        }
    }
}
	






