<?php
require ("config.php");
require ("functions.php");

$posts = get_users();

?>

<? include "inc/header.php";?>
    <div id="content">
    <div id="main">
        <h1>Таблица пользователей</h1>
        <table border="2">
            <tr>
                <td><b>Логин</b></td>
                <td><b>Имя</b></td>
                <td><b>Подтверждение</b></td>
                <td><b>Почта</b></td>
            </tr>
            <? foreach ($posts as $item) :?>
                <tr>
                    <td><?=$item['login'];?></td>
                    <td><?=$item['name'];?></td>
                    <td><?=$item['confirm'];?></td>
                    <td><?=$item['email'];?></td>
                </tr>
            <? endforeach; ?>
        </table>
        <p>
            <a href="admin.php">Назад</a>
        </p>
        <p>
            <a href="users_add.php">Добавить запись</a>
        </p>
    </div>
<? include "inc/sidebar.php";?>

<? include "inc/footer.php";?>