<?php
session_start();
header("Content-Type:text/html;charset=utf8");

require ("config.php");
require ("functions.php");

if (!check_user()){
    header("Location:login.php");
    exit();
}

?>
<? include "inc/header.php";?>
	<div id="content">
		<div id="main">
            <h3>Таблицы: </h3>
            <ul>
                <li><a href="stati_view.php">Cтатьи</a></li><br>
                <li><a href="users_view.php">Пользователи</a></li><br>
            </ul>
		</div>
<? include "inc/sidebar.php";?>		
	
<? include "inc/footer.php";?>


