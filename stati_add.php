<?php
//header("Content-Type:text/html;charset=utf8");

require "config.php";
require "functions.php";

if (isset($_POST['reg'])) {
    $msg = add_statti($_POST);

    if ($msg === TRUE){
        $_SESSION['msg'] = "Статья добавлена";
    } else{
        $_SESSION['msg'] = $msg;
    }
    //header("Location:stati_view.php");
    //exit();
}

?>
<? include "inc/header.php";?>
<div id="content">
    <div id="main">
        <h1>Добавление статьи</h1>
        <?= $_SESSION['msg']; ?>
        <? unset($_SESSION['msg']); ?>

        <form method='POST'>
            Заголовок<br>
            <input type='text' name='title' value="">
            <br>
            Автор<br>
            <input type='text' name='author' value="">
            <br>
            Изображение<br>
            <input type='text' name='img_src' value="">
            <br>
            Содержание<br>
            <input type='text' name='discription' value="">
            <br>
            <input style="float:left" type='submit' name='reg' value='Добавить'>
        </form>
        <br><p>
            <a href="stati_view.php">Назад</a>
        </p>
    </div>
    <? include "inc/sidebar.php";?>

    <? include "inc/footer.php";?>

    <? unset($_SESSION['reg']); ?>


